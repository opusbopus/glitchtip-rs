This project is experimental and not ready for usage. Run with

- cargo run run-server
- cargo run celery-worker
- cargo run celery-beat

or

- `cargo run` runs an all-in-one server for development and simplistic use-cases

# Running locally

- Ensure Rust is installed (use rustup)
- Run a postgres server and set the environment variable DATABASE_URL to it's connection string.
- `cargo run`

## Run tests

`cargo test`

We don't yet support a test database. You'll need to ensure one is running manually.

# Architecture

PostgreSQL is required and redis is recommended but (aims to be) not required.

GlitchTip takes inspiration from Django.

- Axum for web apis
- clap for CLI usage
- SeaORM for ORM queries
  - No database migrations at this time, use the glitchtip-backend django app to create the database.
- Rusty Celery for worker queue (or without redis, a simplistic tokio version is available)
- Runtime configuration is set via environment variables, see src/conf.rs. Implemented via figment and once_cell.
- serde is used for serialization
- Users and Sessions TODO
- Admin API TODO
- OAUTH TODO
