use celery::prelude::*;

#[celery::task]
pub async fn debug_task() -> TaskResult<()> {
    std::thread::sleep(std::time::Duration::from_secs(1));
    println!("Debug task!");
    Ok(())
    }
