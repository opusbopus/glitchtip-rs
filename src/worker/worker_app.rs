use crate::conf;

use super::{
    cleanup_tasks::{cleanup_old_events_task, cleanup_old_files_task},
    tasks::debug_task,
};
use celery::{
    beat::{Beat, CronSchedule, LocalSchedulerBackend},
    Celery,
};
use std::sync::Arc;

static QUEUE_NAME: &str = "celery";

pub async fn get_celery_app() -> Arc<Celery> {
    let settings = &conf::SETTINGS;
    celery::app!(
        broker = RedisBroker { settings.broker_url.clone() },
        tasks = [debug_task, cleanup_old_events_task, cleanup_old_files_task],
        task_routes = [
            "*" => QUEUE_NAME,
        ],
        task_max_retries=1
    )
    .await
    .unwrap()
}

pub async fn get_celery_beat() -> Beat<LocalSchedulerBackend> {
    let settings = &conf::SETTINGS;
    celery::beat!(
        broker = RedisBroker { settings.broker_url.clone() },
        tasks = [
            "cleanup_old_events" => {
                cleanup_old_events_task,
                schedule = CronSchedule::from_string("* * * * *")?,
                args = (),
            },
            "cleanup_old_files" => {
                cleanup_old_files_task,
                schedule = CronSchedule::from_string("* * * * *")?,
                args = (),
            },
        ],
        task_routes = [
            "*" => QUEUE_NAME,
        ],
    )
    .await
    .unwrap()
}
