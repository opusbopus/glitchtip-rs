use celery::prelude::*;
use log::{debug, error, info};
use object_store::path::Path;
use sea_orm::*;
use time::{Duration, OffsetDateTime};

use crate::{conf, storage::get_storage};

pub async fn establish_connection() -> DatabaseConnection {
    let settings = &conf::SETTINGS;
    Database::connect(settings.database_url.clone())
        .await
        .expect("Error connecting to database")
}

#[celery::task]
pub async fn cleanup_old_events_task() -> TaskResult<()> {
    cleanup_old_events().await
}

pub async fn cleanup_old_events() -> TaskResult<()> {
    use crate::entities::{
        events_event, issues_comment, issues_issue, issues_issuehash, user_reports_userreport,
    };

    let settings = &conf::SETTINGS;
    let days = settings.glitchtip_max_event_life_days;
    let days_ago = OffsetDateTime::now_utc() - Duration::days(days.into());
    let conn = establish_connection().await;
    let max_issues = 50000;

    let events_deleted = events_event::Entity::delete_many()
        .filter(events_event::Column::Timestamp.lt(days_ago))
        .exec(&conn)
        .await
        .map_err(|err| {
            error!("Failed to delete old events: {}", err);
            TaskError::UnexpectedError(err.to_string())
        })?;
    info!("Old events deleted: {}", events_deleted.rows_affected);

    // Run in batches of max_issues
    loop {
        let issues_to_delete: Vec<i64> = issues_issue::Entity::find()
            .join(
                JoinType::LeftJoin,
                issues_issue::Relation::EventsEvent.def(),
            )
            .select_only()
            .column(issues_issue::Column::Id)
            .filter(events_event::Column::EventId.is_null())
            .limit(max_issues)
            .into_tuple()
            .all(&conn)
            .await
            .map_err(|err| {
                error!("Failed to load issues to delete: {}", err);
                TaskError::UnexpectedError(err.to_string())
            })?;
        debug!("issues to delete: {:?}", issues_to_delete);

        if issues_to_delete.len() == 0 {
            break;
        }

        let hashes_deleted = issues_issuehash::Entity::delete_many()
            .filter(issues_issuehash::Column::IssueId.is_in(issues_to_delete.clone()))
            .exec(&conn)
            .await
            .map_err(|err| {
                error!("Failed to delete issue hashes: {}", err);
                TaskError::UnexpectedError(err.to_string())
            })?;
        info!("Old issue hashes deleted: {}", hashes_deleted.rows_affected);

        let comments_deleted = issues_comment::Entity::delete_many()
            .filter(issues_comment::Column::IssueId.is_in(issues_to_delete.clone()))
            .exec(&conn)
            .await
            .map_err(|err| {
                error!("Failed to delete issue comments: {}", err);
                TaskError::UnexpectedError(err.to_string())
            })?;
        info!(
            "Old issue comments deleted: {}",
            comments_deleted.rows_affected
        );
        let reports_deleted = user_reports_userreport::Entity::delete_many()
            .filter(user_reports_userreport::Column::IssueId.is_in(issues_to_delete.clone()))
            .exec(&conn)
            .await
            .map_err(|err| {
                error!("Failed to delete issue user reports: {}", err);
                TaskError::UnexpectedError(err.to_string())
            })?;
        info!(
            "Old issue user reports deleted: {}",
            reports_deleted.rows_affected
        );

        let issues_deleted = issues_issue::Entity::delete_many()
            .filter(issues_issue::Column::Id.is_in(issues_to_delete))
            .exec(&conn)
            .await
            .map_err(|err| {
                error!("Failed to load issues to delete: {}", err);
                TaskError::UnexpectedError(err.to_string())
            })?;
        info!("Old issues deleted: {}", issues_deleted.rows_affected);

        if issues_deleted.rows_affected != max_issues {
            break;
        }
    }

    Ok(())
}

#[celery::task]
pub async fn reindex_issues_model() -> TaskResult<()> {
    let conn = establish_connection().await;

    match conn
        .execute_unprepared("REINDEX TABLE CONCURRENTLY issues_issue")
        .await
    {
        Ok(_) => {
            info!("Issues table reindexed");
            Ok(())
        }
        Err(err) => {
            error!("Failed to reindex issues table: {}", err);
            Err(TaskError::UnexpectedError(err.to_string()))
        }
    }
}

#[celery::task]
pub async fn cleanup_old_files_task() -> TaskResult<()> {
    use futures::stream::StreamExt;

    let prefix: Path = "".try_into().unwrap();
    let object_store = get_storage();
    let list_stream = object_store
        .list(Some(&prefix))
        .await
        .expect("Error listing files");
    list_stream
        .for_each(move |meta| async {
            let meta = meta.expect("Error listing");
            println!("Name: {}, size: {}", meta.location, meta.size);
        })
        .await;
    println!("files!");
    Ok(())
}
