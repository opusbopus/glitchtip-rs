use figment::{providers::Env, Figment};
use once_cell::sync::Lazy;
use serde::Deserialize;

fn default_database_url() -> String {
    "postgres://postgres:postgres@localhost:5432/postgres".to_string()
}

fn default_broker_url() -> String {
    "redis://localhost:6379/0".to_string()
}

fn default_as_true() -> bool {
    true
}

fn default_sentry_traces_sample_rate() -> f64 {
    0.01
}

fn default_version() -> String {
    "0.0.0-unknown".to_string()
}

fn default_time_zone() -> String {
    "UTC".to_string()
}

fn default_glitchtip_max_event_life_days() -> u16 {
    90
}

fn default_aws_s3_region_name() -> String {
    "us-east-1".to_string()
}

// GlitchTip settings are a singleton generated at runtime. All settings may be
// configured via environment variables. Example:
// I_PAID_FOR_GLITCHTIP="true" would set i_paid_for_glitchtip to the boolean true
// Some settings are derived from other settings
#[derive(Deserialize, Debug)]
pub struct Settings {
    #[serde(default = "bool::default")]
    pub i_paid_for_glitchtip: bool,
    #[serde(default = "bool::default")]
    pub billing_enabled: bool,
    #[serde(default = "default_broker_url")]
    pub broker_url: String,
    #[serde(default = "default_database_url")]
    pub database_url: String,
    #[serde(default = "default_as_true")]
    pub enable_user_registration: bool,
    #[serde(default = "bool::default")]
    pub enable_organization_registration: bool,
    stripe_test_public_key: Option<String>,
    stripe_live_public_key: Option<String>,
    pub stripe_public_key: Option<String>,
    pub plausible_url: Option<String>,
    pub plausible_domain: Option<String>,
    pub chatwoot_website_token: Option<String>,
    pub sentry_dsn: Option<String>,
    pub sentry_frontend_dsn: Option<String>,
    #[serde(default = "default_sentry_traces_sample_rate")]
    pub sentry_traces_sample_rate: f64,
    pub environment: Option<String>,
    #[serde(default = "default_version")]
    pub version: String,
    #[serde(default = "default_time_zone")]
    pub time_zone: String,
    #[serde(default = "default_glitchtip_max_event_life_days")]
    pub glitchtip_max_event_life_days: u16,

    pub aws_access_key_id: Option<String>,
    pub aws_secret_access_key: Option<String>,
    pub aws_storage_bucket_name: Option<String>,
    pub aws_s3_endpoint_url: Option<String>,
    #[serde(default = "default_aws_s3_region_name")]
    pub aws_s3_region_name: String,
}

impl Settings {
    pub fn new() -> Self {
        let mut settings: Settings = Figment::new().merge(Env::raw()).extract().unwrap();
        settings.billing_enabled =
            settings.stripe_test_public_key.is_some() || settings.stripe_live_public_key.is_some();
        settings.stripe_public_key = settings
            .stripe_test_public_key
            .clone()
            .or(settings.stripe_live_public_key.clone());
        if settings.sentry_frontend_dsn.is_none() {
            settings.sentry_frontend_dsn = settings.sentry_dsn.clone();
        }

        settings
    }
}

pub static SETTINGS: Lazy<Settings> = Lazy::new(|| Settings::new());
