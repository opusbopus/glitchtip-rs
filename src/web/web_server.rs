use axum::Router;

use crate::state::AppState;

use super::{events, organizations, settings};

pub fn get_web_app(state: AppState) -> Router {
    Router::new()
        .merge(settings::views::routes())
        .merge(events::routes())
        .merge(organizations::views::routes())
        .with_state(state)
}
