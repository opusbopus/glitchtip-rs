use axum::{extract::State, http::StatusCode, routing::get, Json, Router};

use crate::{errors::internal_error, state::AppState};
use sea_orm::EntityTrait;

use crate::entities::organizations_ext_organization::{self, Entity as Organization};
use sea_orm::query::*;

pub(crate) fn routes() -> Router<AppState> {
    Router::new().route("/api/0/organizations/", get(organizations_list))
}

async fn organizations_list(
    state: State<AppState>,
) -> Result<Json<Vec<organizations_ext_organization::Model>>, (StatusCode, String)> {
    let organizations = Organization::find()
        .cursor_by(organizations_ext_organization::Column::Created)
        .first(50)
        .all(&state.db)
        .await
        .map_err(internal_error)?;

    Ok(Json(organizations))
}
