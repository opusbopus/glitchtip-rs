use axum::{
    body::Body,
    http::{Request, StatusCode},
};

use crate::{state::get_state, web::web_server::get_web_app};
use tower::ServiceExt;

#[tokio::test]
async fn test_api_settings() {
    let state = get_state().await;

    let app = get_web_app(state);
    let response = app
        .oneshot(
            Request::builder()
                .uri("/api/settings/")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(response.status(), StatusCode::OK);
}
